
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include <stdio.h>

#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>    // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>    // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>  // Initialize with gladLoadGL()
#else
#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
#endif

#include <GLFW/glfw3.h>

#include <array>
#include <algorithm>
#include <iterator>
#include <vector>
#include "DataGenerator.h"
#include "HelperFunctions.h"

#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

static void glfw_error_callback(int error, const char* description)
{
	fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

int main(int, char**)
{
	// Setup window
	glfwSetErrorCallback(glfw_error_callback);
	if (!glfwInit())
		return 1;

#if __APPLE__
	const char* glsl_version = "#version 150";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
	const char* glsl_version = "#version 130";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
#endif

	GLFWwindow* window = glfwCreateWindow(1280, 850, "Programming Task", NULL, NULL);
	if (window == NULL)
		return 1;
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	// Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
	bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
	bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
	bool err = gladLoadGL() == 0;
#else
	bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
	if (err)
	{
		fprintf(stderr, "Failed to initialize OpenGL loader!\n");
		return 1;
	}

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui::StyleColorsDark();

	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);


	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	programming_task::DataGenerator* dg = nullptr;
	static bool show_invalid_data = false;
	static bool show_spl_list = false;
	static int local_avg_threshold = 60;
	
	static double outlier_density = 0.0;

	// Main loop
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();


		{
			static int size = 20;
			ImGui::Begin("Data Generator Settings");
			ImGui::SliderInt("Size", &size, 5, 50);
			if (ImGui::Button("Generate New Data"))
			{
				if (dg != nullptr)
					delete dg;
				dg = new programming_task::DataGenerator(size);
			}

			ImGui::Separator();
			
			ImGui::Spacing();
			ImGui::Checkbox("Show Invalid Data", &show_invalid_data);
			ImGui::Text("Invalid data: 0,1,2,3,4,97,98,99,100");
			
			ImGui::Separator();
			
			ImGui::Spacing();
			ImGui::TextWrapped("Location of 5 consecutive values out of which 3 or more are invalid");
			ImGui::Spacing();
			ImGui::Checkbox("Show", &show_spl_list);

			ImGui::Separator();
			
			ImGui::SliderInt("Local Avg Threshold", &local_avg_threshold, 0, 100);
			ImGui::End();
		}

		{
			ImGui::Begin("Summary");
			ImGui::Text("Outlier Density");
			ImGui::SameLine();
			ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "%.3f %%", outlier_density * 100);
			ImGui::End();
		}

		{
			ImGui::Begin("Data");
			if (dg != nullptr)
			{
				// Show Invalid Data with different Color
				if (show_invalid_data)
				{
					// Get the invalid data positions
					std::vector<int> invalid_pos_list = GetInvalidDataIndices(dg);

					// Get the location of 5 contiguous values with 3 or more invalid data
					std::vector<int> spl_pos_list = GetSplPosIndices(invalid_pos_list);

					// Compute the outlier density
					outlier_density = (double)(invalid_pos_list.size()) / (double)(dg->size());

					ImGui::Columns(dg->width(), NULL, true);
					int overlay_count = 0;

					for (int i = 0; i < dg->size(); ++i)
					{
						if (ImGui::GetColumnIndex == 0)
						{
							ImGui::Separator();
						}

						if (std::find(invalid_pos_list.begin(), invalid_pos_list.end(), i) != invalid_pos_list.end())
							ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "%.3d", (*dg)[i]);
						else
							ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), "%.3d", (*dg)[i]);

						if (std::find(spl_pos_list.begin(), spl_pos_list.end(), i) != spl_pos_list.end())
						{
							overlay_count = 5;
						}

						if (show_spl_list && overlay_count > 0)
						{
							ImGui::GetWindowDrawList()->AddRect(
								ImVec2(ImGui::GetItemRectMin().x - 5, ImGui::GetItemRectMin().y - 2),
								ImVec2(ImGui::GetItemRectMax().x + 5, ImGui::GetItemRectMax().y + 2),
								IM_COL32(255, 255, 0, 255));
							overlay_count -= 1;
						}
						ImGui::NextColumn();
					}
				}
				else
				{
					// Normal Display of Data
					ImGui::Columns(dg->width(), NULL, true);
					for (auto iter = dg->begin(); iter != dg->end(); ++iter)
					{
						if (ImGui::GetColumnIndex == 0)
						{
							ImGui::Separator();
						}
						ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), "%.3d", *iter);
						ImGui::NextColumn();
					}
				}
			}
			ImGui::End();
		}

		{
			ImGui::Begin("Local Average");
			if (dg != nullptr)
			{
				ImGui::Columns(dg->width(), NULL, true);
				for (auto iter = dg->lbegin(); iter != dg->lend(); ++iter)
				{
					if (ImGui::GetColumnIndex == 0)
					{
						ImGui::Separator();
					}

					auto fg_color = *iter >= local_avg_threshold ? ImVec4(0.5f, 0.0f, 0.7f, 1.0f) : ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
					ImGui::TextColored(fg_color, "%.3d", *iter);
					ImGui::NextColumn();
				}
			}
			ImGui::End();
		}

		// Rendering
		ImGui::Render();
		int display_w, display_h;
		glfwMakeContextCurrent(window);
		glfwGetFramebufferSize(window, &display_w, &display_h);
		glViewport(0, 0, display_w, display_h);
		glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwMakeContextCurrent(window);
		glfwSwapBuffers(window);
	}

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}
