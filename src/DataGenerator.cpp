#include "DataGenerator.h"

#include <random>
#include <cassert>

namespace programming_task
{
	DataGenerator::DataGenerator(int size, int high)
	{
		assert(size != 0);

		m_height = m_width = size;
		m_len = size * size;
		m_data = new int[m_len];

		std::random_device rd{};
		std::mt19937 gen{ rd() };
		std::normal_distribution<> d{ high/2.0, high/2.0 };

		for (int i = 0; i < m_len; ++i)
		{
			int value = (int)std::round(std::abs(d(gen)));
			m_data[i] = value % high;
		}
	}

	DataGenerator::~DataGenerator()
	{
		if (m_data != nullptr)
			delete m_data;
	}

	int& DataGenerator::operator[](const int index)
	{
		return m_data[index];
	}

	const int& DataGenerator::operator[](const int index) const
	{
		return m_data[index];
	}
}