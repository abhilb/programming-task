#include "HelperFunctions.h"

std::vector<int> GetInvalidDataIndices(programming_task::DataGenerator* dg)
{
	std::vector<int> invalid_pos_list;
	if (dg != nullptr)
	{
		for (auto iter = dg->begin(); iter != dg->end(); ++iter)
		{
			if (std::find(invalid_data.begin(), invalid_data.end(), *iter) != invalid_data.end())
			{
				invalid_pos_list.push_back(std::distance(dg->begin(), iter));
			}
		}
	}
	return invalid_pos_list;
}

std::vector<int> GetInvalidDataValues(programming_task::DataGenerator* dg)
{
	std::vector<int> invalid_pos_list;
	if (dg != nullptr)
	{
		for (auto iter = dg->begin(); iter != dg->end(); ++iter)
		{
			if (std::find(invalid_data.begin(), invalid_data.end(), *iter) != invalid_data.end())
			{
				invalid_pos_list.push_back(*iter);
			}
		}
	}
	return invalid_pos_list;
}

// Get the location of 5 contiguous values with 3 or more invalid data
std::vector<int> GetSplPosIndices(std::vector<int> & invalid_pos_list)
{	
	std::vector<int> spl_pos_list;
	
	for (auto iter = invalid_pos_list.begin(); iter != invalid_pos_list.end(); ++iter)
	{
		int count = 0;
		for (int i = 0; i < 5; ++i)
		{
			if (iter + i == invalid_pos_list.end())
				break;
			if ((*(iter + i) - *iter) < 5) { ++count; }
		}
		if (count > 2) { spl_pos_list.push_back(*iter); }
	}
	return spl_pos_list;
}
