# programming task

* Generate 2D random data of some size N with values in the range of 0 to 100
* Define Invalid Data as 0,1,2,3,4,97,98,100
* Display all the invalid data
* Show locations where in 5 consecutive values there are 3 or more invalid data
* Show locations where the local average is more than a particular threshold
* Compute the outlier density.

![Screenshot](screenshots/screenshot1.gif)
