#ifndef __helper_functions__
#define __helper_functiosn__

#include "DataGenerator.h"

#include <vector>

const std::vector<int> invalid_data = { 0, 1, 2, 3, 4, 97, 98, 99, 100 };

std::vector<int> GetInvalidDataIndices(programming_task::DataGenerator* dg);
std::vector<int> GetInvalidDataValues(programming_task::DataGenerator* dg);
std::vector<int> GetSplPosIndices(std::vector<int> & invalid_pos_list);

#endif
