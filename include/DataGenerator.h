#ifndef __data_generator__
#define __data_generator__

#include <iterator>

namespace programming_task
{
	class DataGenerator
	{
	public:
		DataGenerator() = delete;
		explicit DataGenerator(int size, int high = 100);
		DataGenerator(const DataGenerator&) = delete;
		DataGenerator& operator=(const DataGenerator&) = delete;
		~DataGenerator();

		int& operator[](const int index);
		const int& operator[](const int index) const;
		
		class Iterator
		{
		public:
			using iterator_category = std::input_iterator_tag;
			using value_type = int;
			using difference_type = int;
			using pointer = int*;
			using reference = int&;

			Iterator(DataGenerator* dg, int index) : m_dg(dg), m_index(index) {}
			
			Iterator& operator++() { m_index += 1; return *this; }
			Iterator operator++(int) { Iterator old(*this); m_index += 1; return *this;	}
			bool operator==(Iterator other) { return this->m_index == other.m_index; }			
			bool operator!=(Iterator other) { return !(*this == other);	}
			value_type operator*() { return (*m_dg)[m_index]; }
			const value_type operator*() const { return (*m_dg)[m_index]; }

		private:
			DataGenerator* m_dg;
			int m_index;
		};

		class LocalAvgIterator
		{
		public:
			using iterator_category = std::input_iterator_tag;
			using value_type = int;
			using difference_type = int;
			using pointer = int*;
			using reference = int&;

			LocalAvgIterator(DataGenerator* dg, int index) : m_dg(dg), m_index(index) {}

			LocalAvgIterator& operator++() { m_index += 1; return *this; }
			LocalAvgIterator operator++(int) { LocalAvgIterator old(*this); m_index += 1; return *this; }
			bool operator==(LocalAvgIterator other) { return this->m_index == other.m_index; }
			bool operator!=(LocalAvgIterator other) { return !(*this == other); }
			value_type operator*() 
			{
				auto row = m_index / m_dg->height();
				auto col = m_index % m_dg->width();

				int sum = 0;
				for (int i = -1; i <= 1; ++i)
				{
					for (int j = -1; j <= 1; ++j)
					{
						if (row + i < 0 || row + i >= m_dg->height())
							continue;

						if(col + j < 0 || col + j >= m_dg->width())
							continue;

						int index = (row + i) * m_dg->width() + (col + j);
						sum += (*m_dg)[index];
					}
				}
				return sum / 9;
			}
			const value_type operator*() const 
			{
				auto row = m_index / m_dg->height();
				auto col = m_index % m_dg->width();

				int sum = 0;
				for (int i = -1; i <= 1; ++i)
				{
					for (int j = -1; j <= 1; ++j)
					{
						if (row + i < 0 || row + i >= m_dg->height())
							continue;

						if (col + j < 0 || col + j >= m_dg->width())
							continue;

						int index = (row + i) * m_dg->width() + (col + j);
						sum += (*m_dg)[index];
					}
				}
				return sum / 9;
			}

		private:
			DataGenerator* m_dg;
			int m_index;
		};

		Iterator begin() { return Iterator(this, 0); }
		Iterator end() { return Iterator(this, m_len); }
		LocalAvgIterator lbegin() { return LocalAvgIterator(this, 0); }
		LocalAvgIterator lend() { return LocalAvgIterator(this, m_len); }

		inline int size() const { return m_len; }
		inline int width() const { return m_width; }
		inline int height() const { return m_height; }
		inline int* data() const { return m_data; }
	private:
		int *m_data;
		int m_len;
		int m_height;
		int m_width;
	};
}

#endif /* __data_generator__ */